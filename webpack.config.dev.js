/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */
const prodConfig = require("./webpack.config");

/** @type {import('webpack').Configuration}  */
module.exports = {
	...prodConfig,
	mode: "development",
	watch: true,
	devtool: "cheap-module-source-map",
	optimization: {},
};
