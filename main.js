/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable no-undef */
// main.js
// Modules to control application life and create native browser window
const { app, BrowserWindow } = require("electron");
const path = require("path");

function createWindow() {
	// Create the browser window.
	const mainWindow = new BrowserWindow({
		width: 800,
		height: 600,
	});

	// and load the index.html of the app.
	mainWindow.loadFile("./dist/index.html");

	// Open the DevTools.
	// mainWindow.webContents.openDevTools()
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Algunas APIs pueden solamente ser usadas despues de que este evento ocurra.
app.whenReady().then(() => {
	createWindow();

	app.on("activate", function () {
		// On macOS it's common to re-create a window in the app when the
		// dock icon is clicked and there are no other windows open.
		if (BrowserWindow.getAllWindows().length === 0) createWindow();
	});
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", function () {
	if (process.platform !== "darwin") app.quit();
});

// In this file you can include the rest of your app's specific main process
// code. Tu también puedes ponerlos en archivos separados y requerirlos aquí.

const env = process.env.NODE_ENV || "development";

if (env === "development") {
	const electronReload = require("electron-reload");

	/** @type {import('electron-reload').ElectronReloadOptions}  */
	const config = {
		electron: path.join(__dirname, "node_modules", ".bin", "electron"),
		hardResetMethod: "exit",
		interval: 1000,
	};

	electronReload(__dirname, config);
}
